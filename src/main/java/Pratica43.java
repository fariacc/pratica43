import java.lang.Math;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

public class Pratica43
{
    public static void main(String[] args)
    {
      Elipse elip = new Elipse(2.5, 1);
      System.out.println(elip.getArea());
      System.out.println(elip.getPerimetro());


      Circulo circ = new Circulo(5);
      System.out.println(circ.getArea());
      System.out.println(circ.getPerimetro());
      
      
      Retangulo ret = new Retangulo(5, 10);
      System.out.println(ret.getArea());
      System.out.println(ret.getPerimetro());
      
      Quadrado quad = new Quadrado(8);
      System.out.println(quad.getArea());
      System.out.println(quad.getPerimetro());
      
      
      TrianguloEquilatero tri = new TrianguloEquilatero(5);
      System.out.println(tri.getArea());
      System.out.println(tri.getPerimetro());
    }
}