package utfpr.ct.dainf.if62c.pratica;

public class TrianguloEquilatero extends Retangulo {
  public TrianguloEquilatero (double lado) {
    super(lado, (lado / 2) * Math.sqrt(3));
  }
  
  @Override
  public double getArea () {
  	return base * altura / 2;
  }
  
    @Override
  public double getPerimetro () {
  	return base * 3;
  }
}