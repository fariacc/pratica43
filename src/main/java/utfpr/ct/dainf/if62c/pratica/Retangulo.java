package utfpr.ct.dainf.if62c.pratica;

public class Retangulo implements FiguraComLados {
  public double base;
  public double altura;
  
  public Retangulo (double _base, double _altura) {
    base = _base;
    altura = _altura;
  }
  
  @Override
  public String getNome() {
  	return this.getClass().getSimpleName();
  }
  
  @Override
  public double getPerimetro() {
  	return 2 * (base + altura);
  }
  
  @Override
  public double getArea() {
  	return base * altura;
  }
  
  @Override
  public double getLadoMenor() {
  	return base > altura ? altura : base;
  }
  
  @Override
  public double getLadoMaior() {
  	return base > altura ? base : altura;
  }
}